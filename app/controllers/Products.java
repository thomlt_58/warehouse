package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import models.Product;
import models.Tag;
import com.avaje.ebean.Ebean;
import play.db.ebean.Model;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.products.details;
import views.html.products.list;
import models.StockItem;


public class Products extends Controller {
    private static final Form<Product> productForm = Form.form(Product.class);

    public static Result newProduct() {
        return ok(details.render(productForm));
    }

    public static Result details(Product product) {

        if (product == null) {
            return notFound(String.format("Product %s does not exist.", product.ean));
        }

        Form<Product> productForm1 = productForm.fill(product);
        return ok(details.render(productForm1));
               //return TODO;
    }

    public static Result list() {
        List<Product> products = Product.findAll();
        return ok(list.render(products));
    }

    public static Result save() {
        Form<Product> boundForm = productForm.bindFromRequest();

        if (boundForm.hasErrors()) {
            flash("error", "Please connect the form bellow.");
            return badRequest(details.render(boundForm));
        }
        Product product = boundForm.get();
        List<Tag> tags = new ArrayList<Tag>();
        for (Tag tag: product.tags) {
            if(tag.id != null) {
                tags.add(Tag.findById(tag.id));
            }
        }

        product.tags = tags;
        if (product.id == null) {
            product.save();
        } else {
            product.update();
        }

        Ebean.save(product);

        StockItem stockItem = new StockItem();
        stockItem.product = product;
        stockItem.quantity = 0L;
        product.save();
        stockItem.save();
        flash("success", String.format("Successfully added product %s", product));
        return redirect(routes.Products.list());
         // save a products
    }

    public static Result delete(String ean) {
        final Product product = Product.findByEan(ean);
        if (product == null) {
            return notFound(String.format("Product %s does not exist.", ean));
        }

        for (StockItem stockItem : product.stockItems) {
            stockItem.delete();
        }
        product.delete();
        return redirect(routes.Products.list());
    }
}