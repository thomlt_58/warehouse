package models;

/**
 * Created by Luu Thom on 3/15/2015.
 */

import play.db.ebean.Model;
import java.util.*;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class StockItem extends Model{
    @Id
    public Long id;
    @ManyToOne
    public Warehouse warehouse;
    @ManyToOne
    public Product product;
    public Long quantity;

    public String toString() {

        return String.format("StockItem %d - %d x product %s",
                id, quantity, product == null ? null : product.id);

    }

    public static Finder<Long, StockItem> find = new Finder<>(Long.class, StockItem.class);
}
