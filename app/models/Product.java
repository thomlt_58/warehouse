package models;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import play.db.ebean.Model;
import play.data.Form;
import play.data.validation.Constraints;
import play.db.ebean.Model.Finder;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;

@Entity
public class Product extends Model implements play.mvc.PathBindable<Product>{

	private static List<Product> products;

    @ManyToMany
    public List<Tag> tags = new LinkedList<Tag>();

    @Id
    public Long id;
    @Constraints.Required
	public String ean;
    @Constraints.Required
	public String name;
    public byte[] picture;
	public String description;
    @OneToMany(mappedBy="product")
    public List<StockItem> stockItems;

    public static Finder<Long,Product> find =
            new Finder<Long,Product>(Long.class, Product.class);

	public Product() {}
	public Product(String ean, String name, String description) {
		this.ean = ean;
		this.name = name;
		this.description = description;
	}
	public String toString () {
		return String.format("%s - %s", ean, name);
	}
	
	public static List<Product> findAll() {
		return find.all();
	}

	public static Product findByEan(String ean) {
		return find.where().eq("ean", ean).findUnique();
	}
 
	public static List<Product> findByName(String term) {
		final List<Product> results = new ArrayList<Product>();
		for (Product candidate : products) {
			if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
				results.add(candidate);
			}
		}
		return results;
	}

    @Override
    public Product bind(String key, String value) {
        return findByEan(value);
    }

    @Override
    public String unbind(String key) {
        return ean;
    }

    @Override
    public String javascriptUnbind() {
        return ean;
    }
}