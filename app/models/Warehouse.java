package models;

/**
 * Created by Luu Thom on 3/15/2015.
 */
import java.util.*;
import play.db.ebean.Model;
import javax.persistence.*;

@Entity
public class Warehouse extends Model {
    @Id
    public Long id;

    public String name;

    @OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList();

    @OneToOne
    public Address address;

    public String toString() {

        return name;
    }

}
